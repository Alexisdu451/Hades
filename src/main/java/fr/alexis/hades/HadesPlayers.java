package fr.alexis.hades;

import java.util.ArrayList;

import fr.alexis.hades.utils.NPC;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class HadesPlayers {
    Hades m;
    public Player p;

    public HadesPlayers(Hades m) {
        this.m = m;
    }


    public int forcefieldTouch = 0;
    public int forcefieldLevel = 0;

    public int flight = 0;
    public int position = 0;
    public int position_look = 0;
    public int look = 0;

    public long lastBowTime = 0L;
    public long lastRegenTime = 0L;
    public long lastBreakEvent = 0L;
    public long lastFightEvent = 0L;

    public static Location currentLocation;

    public Location lastLocation;

    public int walkTicks;

    public int sprintTicks;

    public int JumpTicks;
    public int sneakTicks;
    public static boolean justTeleported = false;
    public static boolean justRespawned = false;
    public static boolean justDied = false;

    public boolean haveBypass(Player player, HadesPlayers wardenPlayer) {

        boolean result = false;

        if (player.getAllowFlight()) {
            result = true;
        }

        if (player.isDead()) {
            result = true;
        }

        if (player.getVehicle() != null) {
            result = true;
        }

        if (justTeleported) {
            justTeleported = false;
            result = true;
        }

        if (justDied) {
            justDied = false;
            result = true;
        }

        if (justRespawned) {
            justRespawned = false;
            result = true;
        }
        return result;
    }

    public void resetTicks() {
        this.walkTicks = 0;
        this.sprintTicks = 0;
        this.JumpTicks = 0;
        this.sneakTicks = 0;
    }

    public int getTotalMoveTicks() {

        return this.walkTicks + this.sprintTicks + this.sneakTicks;
    }

    public ArrayList<NPC> npcs = new ArrayList();

    public HadesPlayers(Player player) {

        this.p = player;
    }

    public Player getPlayer() {

        return this.p;
    }

    public boolean hasByPass() {

        if (isLagging(this.p)) {

            return true;
        }

        return false;
    }

    public String getName() {

        return this.p.getName();
    }

    public HadesPlayers get(Player p) {

        for (HadesPlayers vp : Hades.gp) {

            if (vp.getName().equals(p.getName())) {

                return vp;
            }
        }

        return null;
    }

    public boolean isLagging(Player p) {

        int ping = ((CraftPlayer) p).getHandle().ping;

        if (ping > 120) {

            return true;
        }

        return false;
    }
}
