package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class NoSlowDown implements Listener {
    private FileConfiguration config;
    private static Hades pl;

    public NoSlowDown() {
    }

    public NoSlowDown(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }


    @EventHandler
    public void onslowCheck(FoodLevelChangeEvent e) {
        if (!pl.getConfig().getBoolean("noslowdown")) {
            return;
        }

        if (pl.getConfig().getBoolean("noslowdown")) {
            Player p = (Player) e.getEntity();
            HadesPlayers vp = Hades.get((Player) e.getEntity());
            if (vp.hasByPass()) return;
            if ((p.isSprinting()) && (e.getFoodLevel() > p.getFoodLevel())) {
                e.setCancelled(true);
                Sanction.broadcastModo(p.getPlayer(), CheatType.NOSLOWDOWN);
            }
        }
    }
}
