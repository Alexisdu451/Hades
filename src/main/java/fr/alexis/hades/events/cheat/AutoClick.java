package fr.alexis.hades.events.cheat;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class AutoClick
        implements Listener {
    public static HashMap<UUID, Integer> clickCount = new HashMap();

    public static HashMap<UUID, Double> clickRate = new HashMap();

    @EventHandler
    public void clickBlock(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        if ((event.getAction() == Action.LEFT_CLICK_BLOCK) || (event.getAction() == Action.LEFT_CLICK_AIR)) {
            int newCount = 1;

            if (clickCount.get(event.getPlayer().getUniqueId()) != null) {
                newCount += ((Integer) clickCount.get(event.getPlayer().getUniqueId())).intValue();
            }
            clickCount.put(event.getPlayer().getUniqueId(), Integer.valueOf(newCount));
        }
    }

    public void hacks(Player p, Double str) {
        str = Double.valueOf(Math.round((int) (str.doubleValue() * 100.0D)) / 100);
        for (Player online : Bukkit.getOnlinePlayers()) {
            online.sendMessage(ChatColor.RED + p.getName() + " est entrain de FastClick " + ChatColor.GRAY + "[" + ChatColor.RED + str + ChatColor.GRAY + "] " + ChatColor.RED + "CPS.");
        }
    }
}
