package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;


public class FastBow
        implements Listener {
    @EventHandler
    public void onUseBow(EntityShootBowEvent e) {
        if (e.getForce() != 1.0D) return;
        HadesPlayers p = Hades.get((Player) e.getEntity());
        if (p.hasByPass()) return;
        if (p.lastBowTime == 0L) {
            p.lastBowTime = System.currentTimeMillis();
            return;
        }
        if (System.currentTimeMillis() - p.lastBowTime < 500L) {
            Sanction.broadcastModo(p.getPlayer(), CheatType.FAST_BOW);
        }
        p.lastBowTime = System.currentTimeMillis();
    }
}
