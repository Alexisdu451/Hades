package fr.alexis.hades.events.cheat;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.Sanction;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.NPC;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;


public class ForceField {
    public static int touch = 0;

    public static Player p;
    private FileConfiguration config;
    private static Hades pl;

    public ForceField() {
    }

    public ForceField(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }

    public static void listenNPCDamage() {
        Hades.getProtocolManager().addPacketListener(new PacketAdapter(Bukkit.getPluginManager().getPlugin("Hades"),
                ListenerPriority.NORMAL, new PacketType[]{PacketType.Play.Client.USE_ENTITY}) {

            public void onPacketReceiving(PacketEvent e) {

                if (e.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                    Player pl = e.getPlayer();
                    ForceField.p = pl;
                    try {
                        HadesPlayers
                                gp = Hades.get(ForceField.p);
                        PacketContainer packet = e.getPacket();
                        int id = ((Integer) packet.getIntegers().read(0)).intValue();
                        for (NPC n : gp.npcs) {
                            if (n.getEntityId().intValue() == id) {
                                gp.forcefieldTouch += 1;
                                n.despawn();
                                if (gp.forcefieldTouch >= 2) {
                                    gp.forcefieldLevel += 1;
                                    if (gp.forcefieldLevel >= 2) {
                                        Sanction.broadcastModo(ForceField.p.getPlayer(), CheatType.FORCEFIELD);
                                    }
                                }
                            }
                        }
                    } catch (Exception localException) {
                    }
                }
            }
        });
    }


    public static void checkForceField() {
        new BukkitRunnable() {
            public void run() {
                if (!ForceField.pl.getConfig().getBoolean("forcefield")) {
                    return;
                }

                if (ForceField.pl.getConfig().getBoolean("forcefield")) {
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        HadesPlayers gp = Hades.get(p);
                        gp.forcefieldTouch = 0;
                        int nbplayers = Bukkit.getOnlinePlayers().size();
                        if (nbplayers != 0) {
                            NPC npc1 = new NPC(p, p.getLocation().getBlock().getRelative(-2, 0, 1).getLocation());
                            NPC npc2 = new NPC(p, p.getLocation().getBlock().getRelative(0, 0, 3).getLocation());
                            NPC npc3 = new NPC(p, p.getLocation().getBlock().getRelative(3, 0, 1).getLocation());
                            NPC npc4 = new NPC(p, p.getLocation().getBlock().getRelative(0, 0, -2).getLocation());

                            npc1.spawn("hades-ff1");
                            npc2.spawn("hades-ff2");
                            npc3.spawn("hades-ff3");
                            npc4.spawn("hades-ff4");

                            gp.npcs.add(npc1);
                            gp.npcs.add(npc2);
                            gp.npcs.add(npc3);
                            gp.npcs.add(npc4);
                            ForceField.dispawnNPC(gp);
                        }
                    }
                }
            }
        }.runTaskTimer(Bukkit.getPluginManager().getPlugin("Hades"), 0L, 100L);
    }

    private static void dispawnNPC(HadesPlayers gp) {
        Bukkit.getScheduler().runTaskLater(Bukkit.getPluginManager().getPlugin("Hades"), new Runnable() {
            public void run() {
                for (NPC npc : gp.npcs) {
                    npc.despawn();
                }
            }
        }, 17L);
    }
}

