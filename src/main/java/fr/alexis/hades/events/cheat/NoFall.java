package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import fr.alexis.hades.HadesPlayers;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class NoFall implements org.bukkit.event.Listener {
    private FileConfiguration config;
    private static Hades pl;

    public NoFall() {
    }

    public NoFall(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }


    @EventHandler
    public void onFlyHackChecker(PlayerMoveEvent e) {
        if (!pl.getConfig().getBoolean("nofall")) {
            return;
        }

        if (pl.getConfig().getBoolean("nofall")) {
            Player player = e.getPlayer();
            Location from = e.getFrom().clone();
            Location to = e.getTo().clone();

            Vector vec = new Vector(to.getX(), to.getY(), to.getZ());

            double i = vec.distance(new Vector(from.getX(), from.getY(), from.getZ()));


            if (i == 0.0D) return;
            HadesPlayers vp = Hades.get(e.getPlayer());
            if (player.getGameMode() == GameMode.CREATIVE) return;
            if (player.getVehicle() != null) return;
            if (vp.hasByPass()) {
                return;
            }

            if ((player.getFallDistance() == 0.0F) &&
                    (i > 0.79D) && (player.getPlayer().getHealth() == player.getPlayer().getMaxHealth()) && (player.isOnGround())) {
                e.setCancelled(true);
                player.teleport(player.getLocation(from));
                player.getPlayer().damage(1.0D);
                Sanction.broadcastModo(player.getPlayer(), CheatType.NO_FALL);
            }
        }
    }
}
