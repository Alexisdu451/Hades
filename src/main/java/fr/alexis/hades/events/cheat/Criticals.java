package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import fr.alexis.hades.HadesPlayers;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Criticals implements org.bukkit.event.Listener {
    private FileConfiguration config;
    private static Hades pl;

    public Criticals() {
    }

    public Criticals(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }


    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerJoin(EntityDamageByEntityEvent event) {
        if (!pl.getConfig().getBoolean("criticals")) {
            return;
        }
    
        if (pl.getConfig().getBoolean("criticals")) {
            if (!(event.getDamager() instanceof Player)) {
                return;
            }
            Player player = (Player) event.getDamager();
      
            HadesPlayers vp = Hades.get(player);
            if (vp.hasByPass()) return;
            if (player.getLocation().getBlock().getRelative(BlockFace.DOWN).isLiquid()) return;
            if (player.getLocation().getBlock().getRelative(BlockFace.UP).isLiquid()) {
                return;
            }
            if ((!player.isOnGround()) && (!player.getAllowFlight())) {
                if (player.getLocation().getY() % 1.0D == 0.0D) {
                    if (player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
                        event.setCancelled(true);
                        Sanction.broadcastModo(player.getPlayer(), CheatType.CRITICALS);
                    }
                }
            }
        }
    }
}
