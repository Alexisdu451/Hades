package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import fr.alexis.hades.HadesPlayers;
import org.bukkit.Difficulty;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

public class FastHeal implements Listener {
    private FileConfiguration config;
    private static Hades pl;

    public FastHeal() {
    }

    public FastHeal(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }


    @EventHandler
    public void onRegainHealth(EntityRegainHealthEvent e) {
        if (!pl.getConfig().getBoolean("fastheal")) {
            return;
        }
     
        if (pl.getConfig().getBoolean("fastheal")) {
            if (!(e.getEntity() instanceof Player)) return;
            HadesPlayers p = Hades.get((Player) e.getEntity());
            if ((p.hasByPass()) || (e.getRegainReason() != EntityRegainHealthEvent.RegainReason.SATIATED)) return;
            if (p.lastRegenTime == 0L) {
                p.lastRegenTime = System.currentTimeMillis();
                return;
            }
       
            if (p.getPlayer().getWorld().getDifficulty() == Difficulty.PEACEFUL) {
                return;
            }
            if (System.currentTimeMillis() - p.lastRegenTime < 500L) {
                Sanction.broadcastModo(p.getPlayer(), CheatType.FAST_HEAL);
            }
            p.lastRegenTime = System.currentTimeMillis();
        }
    }
}
