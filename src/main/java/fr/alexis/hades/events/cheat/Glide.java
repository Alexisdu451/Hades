package fr.alexis.hades.events.cheat;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;

public class Glide implements Listener {
    private Map<String, Integer> glideBuffer = new HashMap();
    private Map<String, Double> lastYDelta = new HashMap();
    private Map<String, Double> lastYcoord = new HashMap();


    public static boolean cantStandAtSingle(Block block) {
        Block otherBlock = block.getRelative(BlockFace.DOWN);
        boolean center = otherBlock.getType() == Material.AIR;
        return center;
    }


    public static boolean isInWater(Player player) {
        return (player.getLocation().getBlock().isLiquid()) || (player.getLocation().getBlock().getRelative(BlockFace.DOWN).isLiquid()) || (player.getLocation().getBlock().getRelative(BlockFace.UP).isLiquid());
    }


    public static boolean isInWeb(Player player) {
        return (player.getLocation().getBlock().getType() == Material.WEB) || (player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.WEB) || (player.getLocation().getBlock().getRelative(BlockFace.UP).getType() == Material.WEB);
    }

    @EventHandler
    public void onGlide(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String name = player.getName();
        if (!this.glideBuffer.containsKey(name)) {
            this.glideBuffer.put(name, Integer.valueOf(0));
        }
        if (!this.lastYDelta.containsKey(name)) {
            this.lastYDelta.put(name, Double.valueOf(0.0D));
        }
        double currentY = player.getLocation().getY();
        double math = currentY - ((Double) this.lastYcoord.get(name)).doubleValue();
        if ((math < 0.0D) &&
                (math <= ((Double) this.lastYDelta.get(name)).doubleValue()) && (player.getEyeLocation().getBlock().getType() != Material.LADDER) &&
                (!isInWater(player)) && (!isInWeb(player)) &&
                (cantStandAtSingle(player.getLocation().getBlock()))) {
            int currentBuffer = ((Integer) this.glideBuffer.get(name)).intValue();
            this.glideBuffer.put(name, Integer.valueOf(currentBuffer + 1));
            if (currentBuffer + 1 >= 150) {
                this.lastYDelta.put(name, Double.valueOf(math));
                player.sendMessage("§cGlide !");
            }
        }

        this.lastYDelta.put(name, Double.valueOf(math));
    }
}
