package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.util.Iterator;

public class Flight
        implements Listener {
    private FileConfiguration config;
    private static Hades pl;

    public Flight() {
    }

    public Flight(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }


    @EventHandler
    public void onFlight(PlayerMoveEvent e) {
        if (!pl.getConfig().getBoolean("flight")) {
            return;
        }

        if (pl.getConfig().getBoolean("flight")) {

            Player p = e.getPlayer();

            Location to = e.getTo();
            Location from = e.getFrom();

            Vector vec = new Vector(to.getX(), to.getY(), to.getZ());

            double i = vec.distance(new Vector(from.getX(), from.getY(), from.getZ()));

            if (p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.SPONGE) {
                return;
            }
            if (p.getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }

            Iterator localIterator = p.getActivePotionEffects().iterator();
            if (localIterator.hasNext()) {
                PotionEffect effect = (PotionEffect) localIterator.next();
                return;
            }

            if (e.getPlayer().getEntityId() == 100) {
                return;
            }

            if (e.getPlayer().getVehicle() != null) {
                return;
            }
            HadesPlayers vp = Hades.get(e.getPlayer());
            if (vp.hasByPass()) {
                return;
            }
            if ((p.getFallDistance() == 0.0F) &&
                    (p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.AIR) && (p.getLocation().getBlock().getRelative(BlockFace.UP).getType() == Material.AIR)) {
                if (i > 0.48D) {
                    if (p.isOnGround()) return;
                    e.setCancelled(true);
                    p.teleport(p.getLocation(to));
                    p.teleport(p.getLocation(from));
                    Sanction.broadcastModo(p.getPlayer(), CheatType.Fly);
                }
                if (i > 0.6D) {
                    if (p.isOnGround()) return;
                    e.setCancelled(true);
                    p.teleport(p.getLocation(to));
                    p.teleport(p.getLocation(from));
                    Sanction.broadcastModo(p.getPlayer(), CheatType.Fly);
                }
                if ((i == 0.75D) || (i > 0.74D)) {
                    if (p.isOnGround()) return;
                    e.setCancelled(true);
                    p.teleport(p.getLocation(to));
                    p.teleport(p.getLocation(from));
                    Sanction.broadcastModo(p.getPlayer(), CheatType.HIGHJUMP);
                }
            }


            if ((i > 0.78D) && (i < 0.85D)) {
                if (!p.isOnGround()) return;
                e.setCancelled(true);
                p.teleport(p.getLocation(to));
                p.teleport(p.getLocation(from));
                Sanction.broadcastModo(p.getPlayer(), CheatType.Fly);
            }

            if ((i > 1.25D) || (i == 0.3299999999999983D) || (i == 0.4200000000000017D)) {
                e.setCancelled(true);
                p.teleport(p.getLocation(to));
                p.teleport(p.getLocation(from));
                Sanction.broadcastModo(p.getPlayer(), CheatType.STEP);
            }
            if ((i > 0.28D) && (i < 0.29D)) {
                if (e.getPlayer().getEntityId() == 111) {
                    return;
                }
                if (p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.WATER) return;
                if (p.getLocation().getBlock().getRelative(BlockFace.DOWN).isLiquid()) {
                    e.setCancelled(true);
                    p.teleport(p.getLocation(to));
                    p.teleport(p.getLocation(from));
                    Sanction.broadcastModo(p.getPlayer(), CheatType.JESUS);
                }
            }
        }
    }
}
