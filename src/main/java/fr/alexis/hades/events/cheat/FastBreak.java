package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import fr.alexis.hades.HadesPlayers;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class FastBreak
        implements Listener {
    @EventHandler
    public void onFastBreak(BlockBreakEvent e) {
        Player p1 = e.getPlayer();
        if (e.getBlock().getType() == Material.LONG_GRASS) return;
        if (e.getBlock().getType() == Material.GRASS) return;
        if (e.getBlock().getType() == Material.DIRT) return;
        if (e.getBlock().getType() == Material.SAND) return;
        if (e.getBlock().getType() == Material.SOUL_SAND) return;
        if (e.getBlock().getType() == Material.GRAVEL) return;
        if (e.getBlock().getType() == Material.WHEAT) return;
        if (e.getBlock().getType() == Material.SEEDS) return;
        if (e.getBlock().getType() == Material.COCOA) return;
        if (e.getBlock().getType() == Material.MELON_SEEDS) return;
        if (e.getBlock().getType() == Material.PUMPKIN_SEEDS) return;
        if (e.getBlock().getType() == Material.SUGAR_CANE) return;
        if (e.getBlock().getType() == Material.SUGAR_CANE_BLOCK) return;
        if (e.getBlock().getType() == Material.NETHERRACK) return;
        if (e.getBlock().getType() == Material.DOUBLE_PLANT) return;
        if (e.getBlock().getTypeId() == 38) return;
        if (e.getBlock().getTypeId() == 175) return;
        if (e.getBlock().getTypeId() == 37) return;
        if (p1.getGameMode() == GameMode.CREATIVE) return;
        if (!(e.getPlayer() instanceof Player)) return;
        HadesPlayers p = Hades.get(e.getPlayer());
        if (p.hasByPass()) return;
        if (p.lastBreakEvent == 0L) {
            p.lastBreakEvent = System.currentTimeMillis();
            return;
        }
        if (System.currentTimeMillis() - p.lastBreakEvent < 360L) {
            Sanction.broadcastModo(p.getPlayer(), CheatType.FASTBREAK);
        }

        p.lastBreakEvent = System.currentTimeMillis();
    }
}
