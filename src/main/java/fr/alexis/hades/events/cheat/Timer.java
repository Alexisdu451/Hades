package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;

public class Timer implements org.bukkit.event.Listener {
    private FileConfiguration config;
    private static Hades pl;

    public Timer() {
    }

    public Timer(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }

    private Map<String, Integer> steps = new HashMap();
    private Map<String, Long> stepTime = new HashMap();

    public int increment(Player player, Map<String, Integer> map, int num) {
        String name = player.getName();
        if (map.get(name) == null) {
            map.put(name, Integer.valueOf(1));
            return 1;
        }
        int amount = ((Integer) map.get(name)).intValue() + 1;
        if (amount < num + 1) {
            map.put(name, Integer.valueOf(amount));
            return amount;
        }
        map.put(name, Integer.valueOf(num));
        return num;
    }


    @EventHandler
    public void onSneak(PlayerMoveEvent e) {
        if (!pl.getConfig().getBoolean("speedhack")) {
            return;
        }

        if (pl.getConfig().getBoolean("speedhack")) {
            Player player = e.getPlayer();
            HadesPlayers vp = Hades.get(e.getPlayer());
            if (vp.hasByPass()) {
                return;
            }
            Location to = e.getTo();
            Location from = e.getFrom();
            String name = player.getName();

            int step = 1;
            if (this.steps.containsKey(name)) {
                step = ((Integer) this.steps.get(name)).intValue() + 1;
            }
            if (step == 1) {
                this.stepTime.put(name, Long.valueOf(System.currentTimeMillis()));
            }
            increment(player, this.steps, step);
            if (step == 50) {
                long time = System.currentTimeMillis() - ((Long) this.stepTime.get(name)).longValue();
                this.steps.put(name, Integer.valueOf(0));
                if (time < 2300L) {
                    e.setCancelled(true);
                    player.teleport(player.getLocation(to));
                    player.teleport(player.getLocation(from));
                    Sanction.broadcastModo(player.getPlayer(), CheatType.TIMER);
                }
            }
        }
    }
}