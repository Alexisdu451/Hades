package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

public class Angle implements Listener {
    private FileConfiguration config;
    private static Hades pl;

    public Angle() {
    }

    public Angle(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }

    @EventHandler
    public void onFight(EntityDamageByEntityEvent event) {
        if (!pl.getConfig().getBoolean("angle")) {
            return;
        }
     
        if (pl.getConfig().getBoolean("angle")) {
 
            if ((!(event.getDamager() instanceof Player)) || (!(event.getEntity() instanceof LivingEntity))) return;
            Player player = (Player) event.getDamager();
            LivingEntity damaged = (LivingEntity) event.getEntity();
            double offset = 0.0D;
       
            Location entityLoc = damaged.getLocation().add(0.0D, damaged.getEyeHeight(), 0.0D);
            Location playerLoc = player.getLocation().add(0.0D, player.getEyeHeight(), 0.0D);
       
            Vector playerRotation = new Vector(playerLoc.getYaw(), playerLoc.getPitch(), 0.0F);
            Vector expectedRotation = getRotation(playerLoc, entityLoc);
       
            double deltaYaw = clamp180(playerRotation.getX() - expectedRotation.getX());
            double deltaPitch = clamp180(playerRotation.getY() - expectedRotation.getY());
       
            double horizontalDistance = getHorizontalDistance(playerLoc, entityLoc);
            double distance = getDistance3D(playerLoc, entityLoc);
       
            double offsetX = deltaYaw * horizontalDistance * distance;
            double offsetY = deltaPitch * Math.abs(entityLoc.getY() - playerLoc.getY()) * distance;
       
            offset += Math.abs(offsetX);
            offset += Math.abs(offsetY);
       
            if (offset > 120.0D) {
                event.setCancelled(true);
                Sanction.broadcastModo(player.getPlayer(), CheatType.ANGLE);
            }
        }
    }


    public static Vector getRotation(Location one, Location two) {
        double dx = two.getX() - one.getX();
        double dy = two.getY() - one.getY();
        double dz = two.getZ() - one.getZ();
        double distanceXZ = Math.sqrt(dx * dx + dz * dz);
        float yaw = (float) (Math.atan2(dz, dx) * 180.0D / 3.141592653589793D) - 90.0F;
        float pitch = (float) -(Math.atan2(dy, distanceXZ) * 180.0D / 3.141592653589793D);
        return new Vector(yaw, pitch, 0.0F);
    }


    public static double clamp180(double theta) {
        theta %= 360.0D;
        if (theta >= 180.0D) theta -= 360.0D;
        if (theta < -180.0D) theta += 360.0D;
        return theta;
    }


    public static double getHorizontalDistance(Location one, Location two) {
        double toReturn = 0.0D;
        double xSqr = (two.getX() - one.getX()) * (two.getX() - one.getX());
        double zSqr = (two.getZ() - one.getZ()) * (two.getZ() - one.getZ());
        double sqrt = Math.sqrt(xSqr + zSqr);
        toReturn = Math.abs(sqrt);
        return toReturn;
    }


    public static double getDistance3D(Location one, Location two) {
        double toReturn = 0.0D;
        double xSqr = (two.getX() - one.getX()) * (two.getX() - one.getX());
        double ySqr = (two.getY() - one.getY()) * (two.getY() - one.getY());
        double zSqr = (two.getZ() - one.getZ()) * (two.getZ() - one.getZ());
        double sqrt = Math.sqrt(xSqr + ySqr + zSqr);
        toReturn = Math.abs(sqrt);
        return toReturn;
    }
}
