package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import fr.alexis.hades.HadesPlayers;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerVelocityEvent;

public class AntiKnockBack implements org.bukkit.event.Listener {
    private FileConfiguration config;
    private static Hades pl;

    public AntiKnockBack() {
    }

    public AntiKnockBack(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }


    @org.bukkit.event.EventHandler
    public void onVelocityChange(final PlayerVelocityEvent e) {
        if (!pl.getConfig().getBoolean("antiknockback")) {
            return;
        }

        if (pl.getConfig().getBoolean("antiknockback")) {
            final Player p = e.getPlayer();
            Location loc = new Location(p.getLocation().getWorld(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ());
            if ((p.getLocation().getBlock().getRelative(-1, 0, 0).getType() != Material.AIR) ||
                    (p.getLocation().getBlock().getRelative(1, 0, 0).getType() != Material.AIR) ||
                    (p.getLocation().getBlock().getRelative(0, 0, -1).getType() != Material.AIR) ||
                    (p.getLocation().getBlock().getRelative(0, 0, 1).getType() != Material.AIR)) {

                return;
            }
            final Location hit = p.getLocation();
            HadesPlayers vp = Hades.get(p);
            if (canBypass(p)) return;
            if (vp.hasByPass()) return;
            double force = Math.abs(e.getVelocity().getX() + e.getVelocity().getY() + e.getVelocity().getZ());
            if ((force == 0.0D) || (e.getVelocity().getX() == 0.0D) || (e.getVelocity().getZ() == 0.0D)) return;
            Bukkit.getScheduler().runTaskLater(Bukkit.getPluginManager().getPlugin("Hades"), new Runnable() {
                public void run() {
                    if ((p == null) || (!p.isValid())) return;
                    if (p.getLocation().distanceSquared(hit) < 1.0E-4D) {
                        e.setCancelled(true);
                        Sanction.broadcastModo(p.getPlayer(), CheatType.ANTI_KNOWBACK);
                    }
                }
            }, 3L);
        }
    }

    private boolean canBypass(Player p) {
        Boolean result = Boolean.valueOf(false);
        int ping = ((CraftPlayer) p).getHandle().ping;
        if (ping > 100) result = Boolean.valueOf(true);
        if (p.getVehicle() != null) result = Boolean.valueOf(true);
        return result.booleanValue();
    }
}

