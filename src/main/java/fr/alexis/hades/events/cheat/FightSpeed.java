package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.HashMap;
import java.util.Map;


public class FightSpeed
        implements Listener {
    private FileConfiguration config;
    private static Hades pl;

    public FightSpeed() {
    }

    public FightSpeed(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }

    private Map<String, Long> lastAttack = new HashMap();

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onFight(EntityDamageByEntityEvent event) {
        if (!pl.getConfig().getBoolean("autoclick")) {
            return;
        }

        if (pl.getConfig().getBoolean("autoclick")) {
            if ((!(event.getDamager() instanceof Player)) || (!(event.getEntity() instanceof LivingEntity))) return;
            Player player = (Player) event.getDamager();
            HadesPlayers p = Hades.get((Player) event.getDamager());
            if (p.hasByPass()) return;
            if (p.lastFightEvent == 0L) {
                p.lastFightEvent = System.currentTimeMillis();
                return;
            }

            if (System.currentTimeMillis() - p.lastFightEvent < 90L) {
                event.setCancelled(true);
                Sanction.broadcastModo(p.getPlayer(), CheatType.FIGHT_SPEED);
            }
            p.lastFightEvent = System.currentTimeMillis();
        }
    }
}