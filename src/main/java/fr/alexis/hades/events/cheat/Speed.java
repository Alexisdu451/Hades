package fr.alexis.hades.events.cheat;

import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;

public class Speed implements Listener {
   private Map<String, Long> movingExempt = new HashMap();
   private Map<String, Long> velocitized = new HashMap();
   private Map<String, Integer> speedViolation = new HashMap();

    private boolean isDoing(Player player, Map<String, Long> map, double max) {
        if (map.containsKey(player.getName())) {
            if (max != -1.0D) {
                if ((System.currentTimeMillis() - ((Long) map.get(player.getName())).longValue()) / 1000L > max) {
                    map.remove(player.getName());
                    return false;
                }
                return true;
            }
       
 
            if (((Long) map.get(player.getName())).longValue() < System.currentTimeMillis()) {
                map.remove(player.getName());
                return false;
            }
            return true;
        }
     
 
        return false;
    }

    public boolean isMovingExempt(Player player) {
        return isDoing(player, this.movingExempt, -1.0D);
    }

    public boolean justVelocity(Player player) {
        return System.currentTimeMillis() - ((Long) this.velocitized.get(player.getName())).longValue() < 2100L;
    }

    public boolean isSpeedExempt(Player player) {
        return (isMovingExempt(player)) || (justVelocity(player));
    }

    public int increment(Player player, Map<String, Integer> map, int num) {
        String name = player.getName();
        if (map.get(name) == null) {
            map.put(name, Integer.valueOf(1));
            return 1;
        }
        int amount = ((Integer) map.get(name)).intValue() + 1;
        if (amount < num + 1) {
            map.put(name, Integer.valueOf(amount));
            return amount;
        }
        map.put(name, Integer.valueOf(num));
        return num;
    }


    @EventHandler
    public void onSpeed(PlayerMoveEvent e) {
        double x = 0.0D;
        double z = 0.0D;
        Player player = e.getPlayer();
        if ((!isSpeedExempt(player)) && (player.getVehicle() == null)) {
            String reason = "";
            double max = 0.25D;
            if (player.getLocation().getBlock().getType() == Material.SOUL_SAND) {
                if (player.isSprinting()) {
                    reason = "on soulsand while sprinting ";
                    max = 0.2D;
                } else if (player.hasPotionEffect(PotionEffectType.SPEED)) {
                    reason = "on soulsand with speed potion ";
                    max = 0.16D;
                } else {
                    reason = "on soulsand ";
                    max = 0.13D;
                }
            } else if (player.isFlying()) {
                reason = "while flying ";
                max = 0.56D;
            } else if (player.hasPotionEffect(PotionEffectType.SPEED)) {
                if (player.isSprinting()) {
                    reason = "with speed potion while sprinting ";
                    max = 0.95D;
                } else {
                    reason = "with speed potion ";
                    max = 0.7D;
                }
            } else if (player.isSprinting()) {
                reason = "while sprinting ";
                max = 0.65D;
            }
       
            float speed = player.getWalkSpeed();
            max += (speed > 0.0F ? player.getWalkSpeed() - 0.2F : 0.0F);

            if ((x > max) || (z > max)) {
                int num = increment(player, this.speedViolation, 3);
                if (num >= 3) {
                    Sanction.broadcastModo(player.getPlayer(), CheatType.SPEED_HACK);
                }
            } else {
                this.speedViolation.put(player.getName(), Integer.valueOf(0));
            }
        }
    }
}
