package fr.alexis.hades.events.cheat;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import fr.alexis.hades.utils.CheatType;
import fr.alexis.hades.utils.Sanction;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Reach implements org.bukkit.event.Listener {
    private FileConfiguration config;
    private static Hades pl;

    public Reach() {
    }

    public Reach(Hades gac) {
        pl = gac;
        this.config = pl.getConfig();
    }

    public static double getDistance3D(Location one, Location two) {
        double toReturn = 0.0D;
        double xSqr = (two.getX() - one.getX()) * (two.getX() - one.getX());
        double ySqr = (two.getY() - one.getY()) * (two.getY() - one.getY());
        double zSqr = (two.getZ() - one.getZ()) * (two.getZ() - one.getZ());
        double sqrt = Math.sqrt(xSqr + ySqr + zSqr);
        toReturn = Math.abs(sqrt);
        return toReturn;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onReach(EntityDamageByEntityEvent event) {
        if (!pl.getConfig().getBoolean("reach")) {
            return;
        }

        if (pl.getConfig().getBoolean("reach")) {
            if ((!(event.getDamager() instanceof Player)) || (!(event.getEntity() instanceof LivingEntity))) return;
            Player player = (Player) event.getDamager();
            HadesPlayers vp = Hades.get(player.getPlayer());
            if (vp.hasByPass()) return;
            LivingEntity damaged = (LivingEntity) event.getEntity();
            Location entityLoc = damaged.getLocation().add(0.0D, damaged.getEyeHeight(), 0.0D);
            Location playerLoc = player.getLocation().add(0.0D, player.getEyeHeight(), 0.0D);

            double distance = getDistance3D(entityLoc, playerLoc);
            if (distance > 4.25D) {
                event.setCancelled(true);
                Sanction.broadcastModo(player.getPlayer(), CheatType.REACH);
            }
        }
    }
}
