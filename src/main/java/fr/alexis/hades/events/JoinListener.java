package fr.alexis.hades.events;

import fr.alexis.hades.Hades;
import fr.alexis.hades.HadesPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Hades.getGuardianPlayers().add(new HadesPlayers(event.getPlayer()));
    }
}
