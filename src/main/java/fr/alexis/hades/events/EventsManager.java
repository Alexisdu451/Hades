package fr.alexis.hades.events;

import fr.alexis.hades.Hades;
import fr.alexis.hades.events.cheat.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;


public class EventsManager {
    Hades m;

    public EventsManager(Hades pl) {
        this.m = pl;
    }

    public void registerEvent() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new JoinListener(), this.m);
        pm.registerEvents(new Flight(), this.m);

        pm.registerEvents(new NoSlowDown(), this.m);

        pm.registerEvents(new AntiKnockBack(), this.m);

        pm.registerEvents(new FastBow(), this.m);
        pm.registerEvents(new Criticals(), this.m);
        pm.registerEvents(new NoFall(), this.m);

        pm.registerEvents(new Timer(), this.m);

        pm.registerEvents(new FightSpeed(), this.m);
        pm.registerEvents(new Reach(), this.m);
        pm.registerEvents(new Angle(), this.m);
    }
}
