package fr.alexis.hades;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import fr.alexis.hades.events.EventsManager;
import fr.alexis.hades.events.cheat.*;
import fr.alexis.hades.utils.ManagerProtocol;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Hades extends JavaPlugin {
    public static HashMap<UUID, Integer> clickCount = new HashMap();

    public static HashMap<UUID, Double> clickRate = new HashMap();

    private static ProtocolManager protocolManager;

    public static ArrayList<HadesPlayers> gp = new ArrayList();

    public static Plugin instance;

    public void onLoad() {
        protocolManager = ProtocolLibrary.getProtocolManager();
        reloadConfig();
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.isOp()) {
                p.sendMessage("§8[§2Hades§8]§c §cThe bug can reload !");
            }
        }
    }

    public void onEnable() {
        getConfig().options().copyDefaults(true);
        saveConfig();
        new EventsManager(this).registerEvent();
        new Flight(this);
        new Angle(this);
        new Criticals(this);
        new AntiKnockBack(this);
        new FastHeal(this);
        new FightSpeed(this);
        new ForceField(this);
        new NoFall(this);
        new NoSlowDown(this);
        new Reach(this);
        new Timer(this);

        ManagerProtocol.setupProtocols();

    }


    public void onDisable() {
    }

    public static Plugin getInstance() {
        return instance;
    }

    public static ProtocolManager getProtocolManager() {
        return protocolManager;
    }

    public static List<HadesPlayers> getVirtualPlayers() {
        return gp;
    }

    public static Plugin getPlugin() {
        return Bukkit.getPluginManager().getPlugin("Hades");
    }

    public static List<HadesPlayers> getGuardianPlayers() {
        return gp;
    }

    public static HadesPlayers get(Player player) {
        HadesPlayers s = null;
        for (HadesPlayers gps : gp) {
            if (gps.getPlayer() == player) {
                s = gps;
                break;
            }
        }
        return s;
    }
}
