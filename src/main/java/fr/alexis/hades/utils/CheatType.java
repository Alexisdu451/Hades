
package fr.alexis.hades.utils;


import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ import java.util.Random;

public enum CheatType {
    HIGHJUMP(1, "HighJump", "HighJump", new String[]{"", ""}),
    STEP(2, "Step", "Step", new String[]{"", ""}),
    NOSLOWDOWN(4, "NoSlowDown", "NoSlowDown", new String[]{"", ""}),
    FAST_HEAL(5, "FastHeal", "fast-heal", new String[]{"", ""}),
    FAST_FOOD(6, "Fast-Food-Regen", "fast-food", new String[]{"", ""}),
    ANTI_KNOWBACK(7, "KnockBack", "anti-knowback", new String[]{"", ""}),
    FAST_BOW(8, "FastBow", "faast-bow", new String[]{"", ""}),
    FASTBREAK(9, "FastBreak", "FastBreak", new String[]{"", ""}),
    NO_FALL(10, "NoFall", "NoFall", new String[]{"", ""}),
    SPEED_HACK(11, "Speed", "Speed", new String[]{"", ""}),
    FORCEFIELD(12, "ForceField", "ForceField", new String[]{"", ""}),
    Fly(13, "Fly", "fly", new String[]{"", ""}),
    CRITICALS(14, "Criticals", "criticals", new String[]{"", ""}),
    JESUS(15, "Jesus", "Jesus", new String[]{"", ""}),
    TIMER(16, "SpeedHack", "Timer", new String[]{"", ""}),
    FIGHT_SPEED(16, "AutoClick", "FightSpeed", new String[]{"", ""}),
    ANGLE(16, "Angle", "Angle", new String[]{"", ""}),
    REACH(16, "Reach", "Reach", new String[]{"", ""});

    private String name;
    private String permission;
    private int id;
    private String[] messages;
    private static final Map<String, CheatType> NAME_MAP;
    private static final Map<CheatType, String[]> MESSAGES_MAP;
    private static final Map<Integer, CheatType> ID_MAP;

    private CheatType(int id, String name, String permission, String[] messages) {
        this.name = name;
        this.permission = permission;
        this.id = id;
        this.messages = messages;
    }

    static {
        NAME_MAP = new HashMap();
        MESSAGES_MAP = new HashMap();
        ID_MAP = new HashMap();
        CheatType[] arrayOfCheatType;
        int j = (arrayOfCheatType = values()).length;
        for (int i = 0; i < j; i++) {
            CheatType cheat = arrayOfCheatType[i];
            NAME_MAP.put(cheat.name, cheat);
            ID_MAP.put(Integer.valueOf(cheat.id), cheat);
            MESSAGES_MAP.put(cheat, cheat.messages);
        }
    }

    public CheatType getByName(String name) {
        return (CheatType) NAME_MAP.get(name);
    }

    public CheatType getByID(int id) {
        return (CheatType) ID_MAP.get(this.name);
    }

    public String getPermission() {
        return this.permission;
    }

    public String getName() {
        return this.name;
    }

    public String getMessage() {
        int length = this.messages.length;
        int random = new Random().nextInt(length);
        return this.messages[(random--)] + "§c(" + this.name + ")";
    }
}
