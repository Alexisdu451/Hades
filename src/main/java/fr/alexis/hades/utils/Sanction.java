package fr.alexis.hades.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Sanction {
    private static int tps = 0;

    public static void broadcastModo(Player p, CheatType cheat) {
        for (Player player : Bukkit.getOnlinePlayers()) {


            Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("Hades"), new Runnable() {
                long sec;
                long currentSec;
                int ticks;
                int delay;

                public void run() {
                    this.sec = (System.currentTimeMillis() / 1000L);

                    if (this.currentSec == this.sec) {
                        this.ticks += 1;
                    } else {
                        this.currentSec = this.sec;
                        Sanction.tps = Sanction.tps == 0 ? this.ticks : (Sanction.tps + this.ticks) / 2;
                        this.ticks = 0;
                    }
                }
            }, 0L, 1L);
            int ping = ((CraftPlayer) p).getHandle().ping;

            player.sendMessage(ChatColor.RED + "Hades" + ChatColor.DARK_GRAY + " » " + ChatColor.GOLD + p.getName() + ChatColor.GRAY + " est suspecté de cheat: " + ChatColor.GOLD + cheat.getName() + ChatColor.GRAY + " (ping: " + ChatColor.GOLD + ping + ChatColor.GRAY + " tps: " + ChatColor.GOLD + getTPS() + ChatColor.GRAY + ")");
        }
    }

    public static double getTPS() {
        return TPS.tps;
    }
}
