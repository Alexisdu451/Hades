package fr.alexis.hades.utils;

import com.mojang.authlib.GameProfile;

import java.util.HashMap;
import java.util.UUID;

import fr.alexis.hades.Hades;
import net.minecraft.server.v1_9_R2.EntityPlayer;
import net.minecraft.server.v1_9_R2.MinecraftServer;
import net.minecraft.server.v1_9_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_9_R2.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_9_R2.PlayerConnection;
import net.minecraft.server.v1_9_R2.PlayerInteractManager;
import net.minecraft.server.v1_9_R2.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R2.CraftServer;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class NPC {
    Hades guardian;
    int entityid;
    Player player;
    String name;
    UUID uuid;
    public EntityPlayer npc;
    Location location;

    public NPC(Hades guardian) {
        this.guardian = guardian;
    }

    public static HashMap<Integer, NPC> npcs = new HashMap();

    public NPC(Player player, Location location) {
        this.player = player;
        this.location = location;
    }

    public void spawn(String npc) {
        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) this.player.getWorld()).getHandle();

        this.name = npc;
        this.uuid = UUID.randomUUID();

        this.npc = new EntityPlayer(nmsServer, nmsWorld, new GameProfile(this.uuid, this.name), new PlayerInteractManager(nmsWorld));
        this.npc.setLocation(this.location.getX(), this.location.getY(), this.location.getZ(), 0.0F, 0.0F);
        this.npc.setInvisible(true);

        this.entityid = this.npc.getId();
        PlayerConnection connection = ((CraftPlayer) this.player).getHandle().playerConnection;
        connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, new EntityPlayer[]{this.npc}));
        connection.sendPacket(new net.minecraft.server.v1_9_R2.PacketPlayOutNamedEntitySpawn(this.npc));
        npcs.put(Integer.valueOf(this.npc.getId()), this);
    }

    public void despawn() {
        npcs.remove(Integer.valueOf(this.npc.getId()));
        this.entityid = 0;

        PlayerConnection connection = ((CraftPlayer) this.player).getHandle().playerConnection;
        connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, new EntityPlayer[]{this.npc}));
        connection.sendPacket(new PacketPlayOutEntityDestroy(new int[]{this.npc.getId()}));
    }

    public Integer getEntityId() {
        return Integer.valueOf(this.entityid);
    }

    public Location getLocation() {
        return this.location;
    }
}
