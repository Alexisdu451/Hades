package fr.alexis.hades.utils;

import com.comphenix.protocol.ProtocolManager;
import fr.alexis.hades.Hades;
import fr.alexis.hades.events.cheat.ForceField;


public class ManagerProtocol {
    private static ProtocolManager protocolManager;

    public static void setupProtocols() {
        protocolManager = Hades.getProtocolManager();
        ForceField.checkForceField();
        ForceField.listenNPCDamage();
    }
}
